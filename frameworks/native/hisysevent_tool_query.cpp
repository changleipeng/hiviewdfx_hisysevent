/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hisysevent_tool_query.h"

#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <iosfwd>
#include <iostream>
#include <memory>
#include <ostream>
#include <vector>

#include "hisysevent_json_decorator.h"

namespace OHOS {
namespace HiviewDFX {
void HiSysEventToolQuery::OnQuery(const ::std::vector<std::string>& sysEvent,
    const ::std::vector<int64_t>& seq)
{
    for_each(sysEvent.cbegin(), sysEvent.cend(), [this] (const std::string& tmp) {
        if (this->checkValidEvent && this->eventJsonDecorator != nullptr) {
            std::cout << this->eventJsonDecorator->DecorateEventJsonStr(tmp) << std::endl;
            return;
        }
        std::cout << tmp << std::endl;
    });
}

void HiSysEventToolQuery::OnComplete(int32_t reason, int32_t total)
{
    exit(0);
    return;
}
} // namespace HiviewDFX
} // namespace OHOS
